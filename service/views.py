from pyramid.response import Response
from pyramid.view import view_config

from sqlalchemy.exc import DBAPIError

from .models import (
    DBSession,
    Spot,
    )

# from service.models import Session, Spot
from papyrus.protocol import Protocol

# 'geom' is the name of the mapped class' geometry property
proto = Protocol(DBSession, Spot, 'geom')

@view_config(route_name='spots_read_many', renderer='geojson')
def read_many(request):
    return proto.read(request)

@view_config(route_name='spots_read_one', renderer='geojson')
def read_one(request):
    id = request.matchdict.get('id', None)
    return proto.read(request, id=id)

@view_config(route_name='spots_count', renderer='string')
def count(request):
    return proto.count(request)

@view_config(route_name='spots_create', renderer='geojson')
def create(request):
    return proto.create(request)

@view_config(route_name='spots_update', renderer='geojson')
def update(request):
    id = request.matchdict['id']
    return proto.update(request, id)

@view_config(route_name='spots_delete')
def delete(request):
    id = request.matchdict['id']
    return proto.delete(request, id)

@view_config(route_name='spots_md', renderer='xsd')
def md(request):
    return Spot.__table__
