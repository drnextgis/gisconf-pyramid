from sqlalchemy import (
    Column,
    Integer,
    Unicode,
    )

from sqlalchemy.ext.declarative import declarative_base
from papyrus.geo_interface import GeoInterface
from geoalchemy import *

from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )

from zope.sqlalchemy import ZopeTransactionExtension

DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension()))
Base = declarative_base()

class Spot(GeoInterface, Base):
    __tablename__ = 'july'
    ogc_fid = Column(Integer, primary_key=True)
    confidence = Column(Unicode)
    wkb_geometry = GeometryColumn('wkb_geometry', Point(srid=4326))

GeometryDDL(Spot.__table__)
   

